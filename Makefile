#
# This Makefile requires GNU make.
#
# Do not make changes here.
# Use the included .mak files.
#

make_need := 3.81
ifeq "" "$(strip $(filter $(make_need), $(firstword $(sort $(make_need) $(MAKE_VERSION)))))"
fail := $(error Your make ($(MAKE_VERSION)) is too old. You need $(make_need) or newer)
endif

-include config.mak
include package/targets.mak

INSTALL := ./tools/install.sh

install: install-conf install-lib-scripts install-sysadm-scripts install-service
install-conf: $(CONF_TARGETS:boot.conf=$(DESTDIR)$(boot_conf))
install-lib-scripts: $(SCRIPTS_LIB_TARGETS:scripts/%=$(DESTDIR)$(scripts)/%)
install-sysadm-scripts: $(SCRIPTS_SYSADM_TARGETS:scripts/%=$(DESTDIR)$(sysconfdir)/%)
install-service: $(SERVICES_TARGETS:service/%=$(DESTDIR)$(service_directory)/%)

$(DESTDIR)$(boot_conf): boot.conf
	exec $(INSTALL) -D -m 644 $< $@

$(DESTDIR)$(sysconfdir)/%: scripts/rc.local
	exec $(INSTALL) -D -m 644 $< $@

$(DESTDIR)$(scripts)/%: scripts/%
	exec $(INSTALL) -D -m 755 $< $@
	sed -i -e 's,@BINDIR@,$(bindir),' $@

$(DESTDIR)$(service_directory)/%: service/%
	exec $(INSTALL) -D -m 644 $< $@
	sed -i -e 's,@BOOTCONF@,$(boot_conf),' -e 's,@SCRIPTS@,$(scripts),' \
		-e 's,@INITCONF@,$(init_conf),' $@

version:
	@echo $(version)
	
.PHONY: install version

.DELETE_ON_ERROR:
