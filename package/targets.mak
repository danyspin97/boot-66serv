SCRIPTS_LIB_TARGETS := \
scripts/crypt.awk \
scripts/modules.sh \
scripts/tmpfiles.sh

SCRIPTS_SYSADM_TARGETS := scripts/rc.local

CONF_TARGETS := boot.conf

SERVICES_TARGETS := $(shell find service/boot -type f)

